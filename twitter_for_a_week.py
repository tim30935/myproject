import json
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import tweepy
import datetime

consumer_key = "5JLkf1tyYwCw81DGkIEYuo17l"
consumer_secret = "JL0d2mD5p6jCmRBQZsML3jkTdpOGKsNn5gFmBB6GHnIT7bew3X"
access_token = "968462616229134341-BbZ7Sa7r9lKEEZh2SZd5Svk5XiqgjNL"
access_token_secret = "EDajM2MB2QzAe7FObEUAsav8u7E743yjgZG2lUixPgd4F"
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit = True ) 
#Variables that contains the user credentials to access Twitter API 

fname = 'C:/Users/asus/Desktop/TWITTER/TWITTER_PYTHON/TWITTER_PYTHON/tweets_test3.json'
class Tweet:
    def __init__(self, text, created_at, author, isretweet):
        self.text = text
        self.created_at = created_at
        self.author = author
        self.isretweet = isretweet

    def __str__(self):
        return 'Text: {}\nCreated_At: {}\Author: {}\nIsRetweet: {}\n'.format( self.text, self.created_at, self.author, self.isretweet )

def process_or_store(tweet):
    if 'retweeted_status' in tweet:
    # Retweets 
        tweet_isretweet = True
        if 'extended_tweet' in tweet['retweeted_status']:
           #When extended beyond 140 Characters limit
           tweet_text = tweet['retweeted_status']['extended_tweet']['full_text']
        else:
           tweet_text = tweet['retweeted_status']['full_text']
    else:
    #Normal Tweets
      tweet_isretweet = False
      if 'extended_tweet' in tweet:
      #When extended beyond 140 Characters limit            
        tweet_text = tweet['extended_tweet']['full_text']
      else:
        tweet_text = tweet['full_text']

    tweet_author = tweet['user']['screen_name']
    newTweetObj = Tweet(text=tweet_text, 
                        created_at= datetime.datetime.strptime(tweet['created_at'],'%a %b %d %H:%M:%S %z %Y').strftime('%Y-%m-%d-%H-%M-%S'),
                        author= tweet_author,
                        isretweet = tweet_isretweet)
    with open(fname, mode='a') as file:
              file.write('{}\n'.format(json.dumps(newTweetObj.__dict__)))  



if __name__ == '__main__':
  for tweet in tweepy.Cursor(api.search, 'emerging technologies', lang = 'en', tweet_mode='extended', show_user = True ).items():
    process_or_store(tweet._json)


